/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const int startwithgaps	    = 1;	    /* 1 means gaps are used by default */
static const unsigned int gappx     = 10;       /* default gap between windows in pixels */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "monospace:size=10" };
static const char dmenufont[]       = "monospace:size=10";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char col_lime[]        = "#00e600";
static const char col_dgreen[]      = "#009a00";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1,  col_gray2 },
	[SchemeSel]  = { col_gray4, col_dgreen, col_lime  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     iscentered   isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            0,           1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5;       /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;         /* number of clients in master area */
static const int resizehints = 0;         /* 1 means respect size hints in tiled resizals */

#include "fibonacci.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
 	{ "[@]",      spiral },
 	{ "[\\]",     dwindle },
};

/* Reset number of clients in master area to 1 */
void resetnmaster(const Arg *arg);

/* key definitions */
#define MODKEY Mod1Mask    /* Mod1Msk is mapped in X to left Alt key */
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *termcmd[]          = { "st", NULL };
static const char *browsercmd[]       = { "brave", NULL }; /* Web Browser definition */
static const char *play[]             = { "mocp", "-G", NULL };
static const char *stop[]             = { "mocp", "-x", NULL };
static const char *prev[]             = { "mocp", "-r", NULL };
static const char *next[]             = { "mocp", "-f", NULL };
static const char *mute[]             = { "amixer", "-q", "sset", "Master", "toggle", NULL };
static const char *volumedown[]       = { "amixer", "-q", "sset", "Master", "2%-",  NULL };
static const char *volumeup[]         = { "amixer", "-q", "sset", "Master", "2%+",  NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ControlMask,           XK_t,      spawn,          {.v = termcmd } },
	{ MODKEY|ControlMask,           XK_i,      spawn,          {.v = browsercmd } }, /* Ctrl+Alt+i opens browser */

    /* Music and volume comands */ 
    { MODKEY|ControlMask,           XK_5,      spawn,          {.v = play}},
    { MODKEY|ControlMask,           XK_9,      spawn,          {.v = stop}},
    { MODKEY|ControlMask,           XK_4,      spawn,          {.v = prev}},
    { MODKEY|ControlMask,           XK_6,      spawn,          {.v = next}},
    { MODKEY|ControlMask,           XK_m,      spawn,          {.v = mute}},
    { MODKEY|ControlMask,           XK_Down,   spawn,          {.v = volumedown}},
    { MODKEY|ControlMask,           XK_Up,     spawn,          {.v = volumeup}},

    /* Screen display options */
    { MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ControlMask,           XK_z,      killclient,     {0} },
    /* Layouts */
	{ MODKEY,                       XK_space,  setlayout,      {.v = &layouts[0]} }, /* Alt+Space Tile layout */
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} }, /* Alt+f float layout */
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} }, /* Alt+m Monocle layout "fullscreen" */
	{ MODKEY,                       XK_r,      setlayout,      {.v = &layouts[3]} }, /* Alt+r Fibonacci layout */
	{ MODKEY|ShiftMask,             XK_r,      setlayout,      {.v = &layouts[4]} }, /* Alt+Shift+r Fibonacci going out */
    /* Gaps */ 
	{ MODKEY,                       XK_minus,  setgaps,        {.i = -5 } },
	{ MODKEY,                       XK_plus,   setgaps,        {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_minus,  setgaps,        {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = GAP_TOGGLE} },

    /* Focus and Master control */ 
    { MODKEY,                       XK_o,      resetnmaster,   {0} },                   /* Alt+o to reset the number os clients in master area to 1 */
	{ MODKEY,                       XK_k,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_j,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Tab,    focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_a,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },

    /* Tags */ 
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_5,                      5)
	TAGKEYS(                        XK_5,                      6)
	TAGKEYS(                        XK_5,                      7)
	TAGKEYS(                        XK_5,                      8)
	TAGKEYS(                        XK_5,                      9)

	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },

    /* Restart dwm */ 
	{ MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} }, 
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

/* Reset the number of clients in master area to 1 */
void resetnmaster(const Arg *arg){
    selmon->nmaster = 1;
    arrange(selmon);
}
